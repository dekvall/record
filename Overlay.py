from PyQt5.QtCore import Qt, QPoint
from PyQt5.QtGui import QColor, QPainter, QPaintEvent, QPen, QScreen
from PyQt5.QtWidgets import QApplication, QWidget

from OverlayButtons import OverlayButtons
import util


class Overlay(QWidget):
    def __init__(self, screen: QScreen, start_pos: QPoint, end_pos: QPoint):
        # Qt setup
        super().__init__()
        self.setWindowFlags(
            self.windowFlags()
            | Qt.WindowStaysOnTopHint
            | Qt.FramelessWindowHint
            | Qt.WindowTransparentForInput
            | Qt.Tool
        )
        self.setAttribute(Qt.WA_TranslucentBackground, True)
        self.setWindowTitle("Recorder")
        self.setGeometry(screen.availableGeometry())

        # Window specific setup
        self.dragStart: QPoint = start_pos
        self.dragEnd: QPoint = end_pos
        self.lineStyle = QPen(QColor(255, 0, 0))
        self.lineStyle.setWidth(1)
        self.lineStyle.setStyle(Qt.DashLine)

        # Buttons setup
        self.buttons = OverlayButtons(
            start_pos.x() + screen.availableGeometry().x(),
            end_pos.y() + screen.availableGeometry().y(),
        )
        self.buttons.show()

    def paintEvent(self, e: QPaintEvent):
        c = QPainter()
        c.begin(self)
        c.setRenderHint(QPainter.Antialiasing, True)

        # Draw recording area
        c.setPen(self.lineStyle)
        x, y, w, h = util.getXYWH(self.dragStart, self.dragEnd)
        c.drawRect(x - 1, y - 1, w + 2, h + 2)
        c.end()
