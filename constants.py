import os
import time

RECORD_TIME = time.time()
TMP_FOLDER: str = f"/tmp/record_{RECORD_TIME}/"
FINAL_PATH = os.path.expanduser("~/Pictures/record/")
PREVIEW_CAPTURE_PATH = TMP_FOLDER + "preview.png"
PRE_START_DELAY = 1
INSTALL_DIRECTORY = os.path.dirname(os.path.realpath(__file__))


def init():
    if not os.path.exists(TMP_FOLDER):
        os.mkdir(TMP_FOLDER)
    if not os.path.exists(FINAL_PATH):
        os.mkdir(FINAL_PATH)
