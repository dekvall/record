import asyncio
from asyncqt import QEventLoop
from datetime import datetime
import os
from PIL import Image
from PyQt5.QtWidgets import QApplication
import shutil
import subprocess
import sys
from typing import Callable, List, Tuple

import constants
from exceptions import CancelledException
import util
from Displayer import Displayer
from Overlay import Overlay

# Folder setup
constants.init()

# FFMPEG stuff
ffmpeg = ["ffmpeg", "-hide_banner", "-loglevel", "panic", "-async", "1", "-vsync", "1"]
ffmpeg_debug = ["ffmpeg", "-hide_banner", "-async", "1", "-vsync", "1"]


def filename(format: str) -> str:
    return f"record_{datetime.utcfromtimestamp(constants.RECORD_TIME).strftime('%Y-%m-%d__%H-%M-%S')}.{format}"


# Return framerate data, encode settings, and output name
def ffmpeg_gif_settings() -> Tuple[List[str], List[str], List[str], str]:
    return (
        ["-framerate", "30"],
        [],
        ["-filter_complex", "palettegen=stats_mode=full[p]; [0][p]paletteuse"],
        f"{constants.TMP_FOLDER}out.gif",
    )


def ffmpeg_video_settings() -> Tuple[List[str], List[str], List[str], str]:
    return (
        ["-framerate", "60"],
        [
            "-init_hw_device",
            "vaapi=amd:/dev/dri/renderD128",
            "-hwaccel",
            "vaapi",
            "-hwaccel_output_format",
            "vaapi",
            "-hwaccel_device",
            "amd",
        ],
        [
            "-filter_hw_device",
            "amd",
            "-vf",
            "format=nv12|vaapi,hwupload",
            "-c:v",
            "h264_vaapi",
            "-preset",
            "medium",
            "-profile:v",
            "578",
        ],
        f"{constants.TMP_FOLDER}out.mp4",
    )


def arg_parse(
    arg: str,
) -> Tuple[str, None or Callable[[], Tuple[List[str], List[str], List[str], str]]]:
    arg = arg.lower()
    if arg in ["gif"]:
        return "gif", ffmpeg_gif_settings
    elif arg in ["video"]:
        return "mp4", ffmpeg_video_settings
    elif arg in ["still"]:
        return "png", None


async def record(
    overlay: Overlay,
    framerate_setting: List[str],
    encode_input_settings: List[str],
    encode_settings: List[str],
    name: str,
    x: int,
    y: int,
    w: int,
    h: int,
):
    waited = 0
    while waited < constants.PRE_START_DELAY:
        overlay.update()
        overlay.buttons.update()
        await asyncio.sleep(0.01)
        waited += 0.01

    env = os.environ.copy()
    env["VAAPI_MPEG4_ENABLED"] = "1"
    # Quality magic. Records raw video with one FFMPEG, then passes it into another to encode it
    # This results in gifs with no colour loss, and offloads the MPEG encoding of Video to the GPU safely
    with subprocess.Popen(
        [
            *ffmpeg,
            *framerate_setting,
            "-video_size",
            f"{w}x{h}",
            "-f",
            "x11grab",
            "-i",
            f":0.0+{x},{y}",
            "-c:v",
            "rawvideo",
            "-f",
            "nut",
            "-",
        ],
        stdout=subprocess.PIPE,
        env=env,
    ) as cap:
        with subprocess.Popen(
            [*ffmpeg, *encode_input_settings, "-i", "-", *encode_settings, name],
            stdin=cap.stdout,
        ) as enc:
            # Convenient way to both keep the overlay updating, and to end the recording
            while overlay.buttons.capture_running:
                overlay.update()
                overlay.buttons.update()
                await asyncio.sleep(0.01)

            if overlay.buttons.cancelled:
                cap.terminate()
                enc.terminate()
                overlay.close()
                overlay.buttons.close()
                enc.wait()
                cap.wait()
            else:
                overlay.close()
                # Tell the capturer to stop, then wait for it to finish
                cap.terminate()
                cap.wait()
                # Encoder will stop on its own when it runs out of input
                enc.wait()
                overlay.buttons.close()


# Use a try/finally to make sure the temp environment is cleaned
try:
    qApp = QApplication(sys.argv)
    # I am unbelievably happy that this library exists
    loop: QEventLoop = QEventLoop(qApp)
    asyncio.set_event_loop(loop)

    displayer = Displayer(qApp.screens())
    displayer.showFullScreen()
    qApp.exec_()
    if displayer.cancel:
        raise CancelledException()

    format, settings_func = arg_parse(sys.argv[1])
    # Offset the coords by the screen's position, so FFMPEG and maim use the correct area
    top_left = displayer.drag_start + displayer.screen.availableGeometry().topLeft()
    bottom_right = displayer.drag_end + displayer.screen.availableGeometry().topLeft()
    if format == "png":
        image = Image.open(constants.PREVIEW_CAPTURE_PATH)
        image = image.crop(util.getRect(displayer.drag_start, displayer.drag_end))
        image.save(f"{constants.FINAL_PATH}{filename(format)}", "PNG")
        subprocess.run(
            [
                "xclip",
                "-selection",
                "clipboard",
                "-t",
                "image/png",
                "-i",
                f"{constants.FINAL_PATH}{filename(format)}",
            ]
        )
    else:
        framerate, encode_in, encode, name = settings_func()

        overlay = Overlay(displayer.screen, displayer.drag_start, displayer.drag_end)
        overlay.showFullScreen()

        with loop:
            loop.run_until_complete(
                record(
                    overlay,
                    framerate,
                    encode_in,
                    encode,
                    name,
                    *util.getXYWH(top_left, bottom_right),
                )
            )

        if overlay.buttons.cancelled:
            raise CancelledException()
        # Move the video to its new home
        os.rename(name, f"{constants.FINAL_PATH}{filename(format)}")
        subprocess.run(
            [
                "xclip",
                "-selection",
                "clipboard",
                "-t",
                "text/uri-list",
                "-t",
                "MULTIPLE",
                "-i",
                f"{constants.FINAL_PATH}{filename(format)}",
            ]
        )
finally:
    # Clean up the temp folder
    shutil.rmtree(constants.TMP_FOLDER)
