from PyQt5.QtCore import Qt, QPoint, QRect
from PyQt5.QtGui import (
    QBrush,
    QColor,
    QCursor,
    QImage,
    QKeyEvent,
    QPainter,
    QPaintEvent,
    QPen,
    QMouseEvent,
    QScreen,
)
from PyQt5.QtWidgets import QApplication, QWidget
from typing import List

import constants
import util


class Displayer(QWidget):
    def __init__(self, screens: List[QScreen]):
        super().__init__()

        # Get the current screen as an image
        screen_index, self.screen = util.get_current_screen(QCursor().pos(), screens)
        if self.screen is None:
            self.cancel = True
            self.close()
        util.capture_screen(screen_index)

        # Qt setup
        self.setWindowFlags(
            self.windowFlags()
            | Qt.WindowStaysOnTopHint
            | Qt.FramelessWindowHint
            | Qt.Tool
        )
        self.setWindowTitle("Select a region")
        self.setGeometry(self.screen.availableGeometry())

        # Window specific setup
        self.kill = False
        self.image_to_use: QImage = QImage(constants.PREVIEW_CAPTURE_PATH)
        self.dragging = False
        self.drag_start: QPoint = None
        self.drag_end: QPoint = None
        self.line_style = QPen(QColor(255, 0, 0))
        self.line_style.setWidth(1)
        self.line_style.setStyle(Qt.DashLine)
        self.cancel = False

    def paintEvent(self, e: QPaintEvent):
        c = QPainter()
        c.begin(self)
        c.setRenderHint(QPainter.Antialiasing, True)

        # Draw preview
        c.drawImage(QPoint(0, 0), self.image_to_use)
        c.fillRect(
            QRect(0, 0, self.image_to_use.width(), self.image_to_use.height()),
            QBrush(QColor(0, 0, 0, 80)),
        )

        # Draw selection box
        if self.drag_start is not None and self.drag_end is not None:
            c.setPen(self.line_style)
            c.drawImage(
                self.drag_start,
                self.image_to_use,
                QRect(*util.getXYWH(self.drag_start, self.drag_end)),
            )
            c.drawRect(*util.getXYWH(self.drag_start, self.drag_end))
        c.end()

    def keyPressEvent(self, event: QKeyEvent):
        if event.key() == Qt.Key_Escape:
            self.close()

    def mousePressEvent(self, e: QMouseEvent):
        if e.button() == 1:
            self.dragging = True
            self.drag_start = e.pos()
        elif e.button() == 2:
            self.cancel = True
            self.close()

    def mouseReleaseEvent(self, e: QMouseEvent):
        if e.button() == 1:
            self.dragging = False
            self.drag_end = e.pos()
            self.close()

    def mouseMoveEvent(self, e: QMouseEvent):
        if self.dragging:
            self.drag_end = e.pos()
            self.repaint()

    def closeEvent(self, e):
        e.accept()
        # This is needed to have a Qt.Tool window actually close, for some reason
        QApplication.exit(0)
